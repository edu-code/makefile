.PHONY: all lint pretty clean

all: test

check: lint test radon bandit

# We use both becase they find fixes after each other
pretty: autopep8 yapf
	
lint:
	flake8

test: 
	coverage run --branch --omit="venv/*,tests/*,tests.py" tests.py && \
	coverage report && \
	coverage html --fail-under=70 && \
	coverage erase

bandit:
	bandit -r --exclude='./venv/,./migrations/' .

clean:
	rm -rf venv
	rm -rf htmlcov
	find -iname "*.pyc" -delete
	find -type d -name __pycache__ -delete
